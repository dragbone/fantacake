package ch.cake.fanta;

/**
 * Created by dragbone on 29.08.2016.
 */
public class FantaCakeFillingFactory{
	private static final float CHOSEN_HIGHER_FAT_CONTENT = 0.5f;
	
	public CakeFilling createFantaCakeFilling(){
		Dice<Fruit> dicedFruit = new Dicer().dice(new Ingredient(IngredientType.Orange, Unit.fromNumber(2)));

		MixingStrategy bowl = new StirMixingStrategy();

		Mixture creamFruitMixture = bowl
				.mix(dicedFruit, new Ingredient(IngredientType.SourCream, Unit.fromGrams((int)(300*(1-CHOSEN_HIGHER_FAT_CONTENT)))),
						new Ingredient(IngredientType.CremeFraiche, Unit.fromGrams((int)(300*CHOSEN_HIGHER_FAT_CONTENT))),
						new Ingredient(IngredientType.VanillaSugar, Unit.fromPackage(1)));

		IngredientWhipper whipper = new IngredientWhipper();
		Mixture whippedCream = whipper.whip(new Ingredient(IngredientType.Cream, Unit.fromGrams(360)),
				new Ingredient(IngredientType.VanillaSugar, Unit.fromPackage(2)),
				new Ingredient(IngredientType.WhippingCreamStabilizer, Unit.fromPackage(2)));

		Mixture fillingMixture = bowl.mix(creamFruitMixture, whippedCream);

		return new CakeFilling(fillingMixture);
	}
}

/* INTERNAL STUFF */
class Dicer{
	public Dice<Fruit> dice(Ingredient... i){
		return null;
	}
}

class Fruit {
}

class Dice<T> implements Mixture{

}