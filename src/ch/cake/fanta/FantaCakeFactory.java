package ch.cake.fanta;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * Created by dragbone on 29.08.2016.
 */
class FantaCakeFactory{
	private static final float MIN_CONSISTENCY = 0.7f;

	public FantaCake createFantaCake() throws ExecutionException, InterruptedException{
		Future<CakeBase> cakeBaseFuture = new FantaCakeBaseFactory().createFantaCakeBase();
		CakeFilling cakeFilling = new FantaCakeFillingFactory().createFantaCakeFilling();
		FantaCake cake = new FantaCakeAssembler(cakeBaseFuture.get(), cakeFilling).assemble(new StackingStrategy());

		/* Required to provide Lukas-compatibility (see issue #23) */
		cake = new FantaCakeMalteserDecorator(cake);

		Freezer freezer = new Freezer();
		while(cake.getConsistency() < MIN_CONSISTENCY){
			freezer.cool(cake);
		}
		return cake;
	}
}

/* INTERNAL STUFF */
class FantaCakeAssembler{
	private final CakeLayer cakeBase, cakeFilling;

	public FantaCakeAssembler(CakeBase cakeBase, CakeFilling cakeFilling){
		this.cakeBase = cakeBase;
		this.cakeFilling = cakeFilling;
	}

	public FantaCake assemble(StackingStrategy stackingStrategy){
		return null;
	}
}

class Freezer{
	public <T> void cool(T target){

	}
}

class FantaCakeMalteserDecorator extends FantaCake{
	FantaCakeMalteserDecorator(FantaCake cake){
	}
}

