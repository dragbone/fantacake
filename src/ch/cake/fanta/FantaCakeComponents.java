package ch.cake.fanta;

/* INTERNAL STUFF */

class CakeLayer{
}

class CakeBase extends CakeLayer{
}

class CakeFilling extends CakeLayer{
	public CakeFilling(Mixture fillingMixture){
	}
}

class FantaCake{
	float getConsistency(){
		return 0f;
	}
}

class StackingStrategy{
	/*public T stack(CakeLayer... layers){
		return null;
	}*/
}

class Unit{
	static Unit fromNumber(int n){
		return null;
	}

	static Unit fromGrams(int n){
		return null;
	}

	static Unit fromPackage(int n){
		return null;
	}

	static Unit fromMilliliters(int n){
		return null;
	}
}

enum IngredientType{Egg, Sugar, VanillaSugar, Flour, BakingSoda, Oil, Fanta, Orange, SourCream, CremeFraiche, Cream, WhippingCreamStabilizer}

class IngredientWhipper{
	Mixture whip(Mixture... is){
		return null;
	}
}

class Ingredient implements Mixture{
	public Ingredient(IngredientType type, Unit unit){
	}
}

interface Mixture{
}

interface MixingStrategy{
	public Mixture mix(Mixture... is);
}

class StirMixingStrategy implements MixingStrategy{
	@Override public Mixture mix(Mixture... is){
		return null;
	}
}
