package ch.cake.fanta;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * Created by dragbone on 29.08.2016.
 */
public class FantaCakeBaseFactory{
	private static final float MAX_DOUGH_HEIGHT_CM = 2.0f;
	
	public Future<CakeBase> createFantaCakeBase() throws ExecutionException, InterruptedException{
		IngredientWhipper whipper = new IngredientWhipper();

		Mixture whippedMixture = whipper.whip(new Ingredient(IngredientType.Egg, Unit.fromNumber(3)),
				new Ingredient(IngredientType.Sugar, Unit.fromGrams(180)),
				new Ingredient(IngredientType.VanillaSugar, Unit.fromPackage(1)));

		MixingStrategy bowl = new StirMixingStrategy();

		Mixture flourMixture = bowl.mix(new Ingredient(IngredientType.Flour, Unit.fromGrams(210)),
				new Ingredient(IngredientType.BakingSoda, Unit.fromPackage(1)));

		Mixture stickyMixture = bowl.mix(whippedMixture, flourMixture);

		Mixture dough = bowl.mix(stickyMixture, new Ingredient(IngredientType.Oil, Unit.fromMilliliters(75)),
				new Ingredient(IngredientType.Fanta, Unit.fromMilliliters(105)));

		Oven oven = new Oven();
		oven.preheatDegrees(150).wait();
		Future<CakeBase> cakeBaseFuture = oven.bake(dough, 25, TimeUnit.MINUTES, MAX_DOUGH_HEIGHT_CM);
		return cakeBaseFuture;
	}
}

/* INTERNAL STUFF */
class Oven{
	public Object preheatDegrees(int i){
		return this;
	}

	public Future<CakeBase> bake(Mixture dough, int i, TimeUnit minutes, float maxDoughHeight){
		return null;
	}
}

